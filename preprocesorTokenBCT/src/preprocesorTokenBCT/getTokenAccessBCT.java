package preprocesorTokenBCT;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.konylabs.middleware.api.OperationData;
import com.konylabs.middleware.api.ServiceRequest;
import com.konylabs.middleware.common.DataPreProcessor2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Result;

public class getTokenAccessBCT implements DataPreProcessor2 {
	private static final Logger log = Logger
			.getLogger(getTokenAccessBCT.class);
	/**
	 * Desarrollado por: Ing.Edgar Jimenez - Consultor Senior Descripcion: consultar
	 * servicio get token y agregar en el request header del servicio en curso
	 * Observacion: para implementar cambiar log.error por log.debug, los log.error
	 * permiten ver la traza haciendo pruebas desde MF web.
	 */
	@Override
	public boolean execute(HashMap Map, DataControllerRequest Request, DataControllerResponse Response, Result Result)
			throws Exception {
		String mensajeGenerico = "SYBVEN - tokenAccessBCT - ";
		try {

			String idUsuario = "";
			String idioma = "";
			String IP = "";
			String canal = "";
			boolean isUserID = false;

			log.error(mensajeGenerico + "MOSTRAR TODO");
			log.error(Request);
			log.error(Map);
			log.error(Result);
			log.error(mensajeGenerico + "INICIAR FLUJO");
			/********************************************************/
			log.error(mensajeGenerico + "Cargar Service Data");
			OperationData serviceData = Request.getServicesManager().getOperationDataBuilder()
					.withServiceId("dbpProductServicesJSONBCT").withOperationId("obtenerToken").build();
			log.error(mensajeGenerico + "Service Data Cargado");
			/***********************************************************/
			Map<String, Object> header = new HashMap<>();
			Map<String, Object> inputParam = new HashMap<>();
			// Agregar parametros necesarios para el consumo de get token.
			log.error(mensajeGenerico + "COLOCAR PARAMETROS");
			try {

				log.error(mensajeGenerico + "ASIGNAR DATA TOKEN");

				/**
				 * VALIDACION PARA BUSCAR EL PARAMETRO DE ID del usuario dependiendo del request.
				 */
				try {
					idUsuario = (String) Map.get("idUsuario");
					if(idUsuario != "" && idUsuario != null){
						isUserID = true;
					}
				} catch (Exception error1) { log.error(mensajeGenerico + "No encontrado idUsuario"); }
				try {
					if(isUserID == false){
						idUsuario = (String) Map.get("userId");
					}
				} catch (Exception error2) { log.error(mensajeGenerico + "No encontrado userId"); }

				idioma = (String) Map.get("idioma");
				canal = (String) Map.get("canal");
				IP = (String) Map.get("ip");
			} catch (Exception e) {
				log.error(mensajeGenerico + "EXPLOTO SM: " + e);
			}
			inputParam.put("codigoAplicacion", "TEMENOS:APP:001");
			inputParam.put("idUsuario", idUsuario);
			inputParam.put("idioma", idioma);
			inputParam.put("IP", IP);
			inputParam.put("canal", canal);
			log.error(inputParam);
			log.error(mensajeGenerico + "INICIO invocacion de servicio ASIGNADO ARRIBA");
			ServiceRequest serviceRequest = Request.getServicesManager().getRequestBuilder(serviceData)
					.withInputs(inputParam).withHeaders(header).build();
			String response = serviceRequest.invokeServiceAndGetJson();
			log.error(mensajeGenerico + "FINAL invocacion de servicio ASIGNADO ARRIBA");
			/***************************
			 * Response pruebas edgar "{\"opstatus\":0,\"token\":\"Este es mi
			 * token\",\"httpStatusCode\":200}"
			 *******************************/
			log.error(mensajeGenerico + "INICIO OBTENER RESPUESTA JSON");
			JSONObject jsonResp = new JSONObject(response);
			log.error(mensajeGenerico + "FINAL OBTENER RESPUESTA JSON");
			/**********
			 * Buscar parametro token en la respuesta del servicio get token
			 *******************/
			log.error(mensajeGenerico + "DATA JSON MOSTRAR");
			log.error(jsonResp);
			// int opstatusToken = Integer.parseInt(jsonResp.getString("opstatus"));
			/*******************************************************************************************/
			String TOKEN = jsonResp.getString("token");
			/*********************** SETENADO TOKEN EN HEADER **************************/
			Map<String, Object> HeaderRequest = Request.getHeaderMap();
			HeaderRequest.put("token", TOKEN);
			/*********************
			 * Nota el parametro debe estar mapeado en header del servicio
			 *****************/
			log.error(mensajeGenerico + " FIN EJECUCION");
			return true;

		} catch (Exception e) {
			log.error(mensajeGenerico + "EXPLOTO TRY: " + e);
			return false;
		}
	}
}
